// ignore_for_file: must_be_immutable, void_checks

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:friver_app/blocs/driver_model_cubit.dart';
import 'package:friver_app/data/models/driver_model.dart';
import 'package:friver_app/data/repositories/driver_repository_interface.dart';
import 'package:friver_app/locator.dart';
import 'package:friver_app/ui/widgets/custom_elevated_button.dart';
import 'package:friver_app/ui/widgets/custom_positioned.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);
  static const name = "home-page";

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => DriverModelCubit(getIt.get<DriverRepositoryInterface>()),
      child: HomePageView(),
    );
  }
}

class HomePageView extends StatelessWidget {
  bool pendency = false;
  bool load = false;
  DriverModel? driver;
  String name = "";
  String id = "";

  //Image qr;

  HomePageView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<DriverModelCubit, DriverState>(
      listener: (context, state) {
        if (state is LoadingState) {
          const Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state is ErrorState) {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text('Algo deu errado, tente novamente!')));
        }
        if (state is SuccessPendencyState) {
          final pen = state.pendency;
          if (pen.orders.isNotEmpty && pen.boxes == 0) {
            pendency = true;
          }
        }
        if (state is SuccesLoadState) {
          final lod = state.loads;
          if (lod!.total != 0) {
            load = true;
          }
        }
        if (state is SuccessState) {
          driver = state.driver;
          name = driver!.data.firstName + " " + driver!.data.lastName;
          id = driver!.iD;
        }
        /*if (state is SuccessQRCodeState) {
          qr = state.qrc!.qrCode;
        }*/
      },
      child: BlocBuilder<DriverModelCubit, DriverState>(
        builder: (context, state) {
          return Scaffold(
            drawer: _buildDrawer(context),
            appBar: AppBar(
              iconTheme: const IconThemeData(color: Colors.black),
              elevation: 0,
              backgroundColor: Colors.grey.shade200,
              title: const Center(
                child: Text("Facily Driver",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        fontWeight: FontWeight.bold)),
              ),
              actions: [
                IconButton(
                  onPressed: () {
                    context.read<DriverModelCubit>().getDriver();
                    context.read<DriverModelCubit>().getDriverLoad();
                    context.read<DriverModelCubit>().getPendency();
                  },
                  icon: const Icon(Icons.autorenew),
                )
              ],
            ),
            body: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Olá, $name",
                      style: const TextStyle(
                          fontSize: 18, fontWeight: FontWeight.bold)),
                  const Text("Tudo bem?",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w300)),
                  const SizedBox(height: 15),
                  Visibility(
                    visible: pendency,
                    child: CustomPositioned(
                        color: Colors.lightGreen.shade200,
                        children: const <Widget>[
                          Text("Carregamento Liberado",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.bold)),
                          Text(
                              "Dirija-se ao setor responsável e faça um novo carregamento",
                              style: TextStyle(
                                fontSize: 13,
                                fontWeight: FontWeight.w100,
                              )),
                        ]),
                  ),
                  const SizedBox(height: 15),
                  Visibility(
                    visible: load,
                    child: CustomPositioned(
                      color: Colors.grey.shade200,
                      children: const <Widget>[
                        Text("O que você deseja?",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold)),
                        Text("Selecione a opção abaixo",
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w200,
                              color: Colors.grey,
                            )),
                        SizedBox(height: 10),
                        CustomElebatedButton(text: "Verificar carregamentos"),
                        SizedBox(height: 15),
                        CustomElebatedButton(text: "Entregar pedidos"),
                        SizedBox(height: 15),
                        CustomElebatedButton(text: "Consultar NFe"),
                      ],
                    ),
                  ),
                  const SizedBox(height: 15),
                  CustomPositioned(color: Colors.grey.shade200, children: [
                    Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text("N° de identificação:",
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300,
                                  )),
                              Text(id,
                                  style: const TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)),
                              const SizedBox(height: 15),
                              const Text(
                                  "Ultilize o QRCode sempre que for necessario confirmar sua identificação",
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w100,
                                    color: Colors.grey,
                                  )),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Image.network(
                                  'https://southamerica-east1-facily-817c2.cloudfunctions.net/generate_qr_code?data=$id')
                            ],
                          ),
                        ),
                      ],
                    )
                  ])
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

Drawer _buildDrawer(BuildContext context) {
  return Drawer(
    child: ListView(
      children: [
        DrawerHeader(
          decoration: const BoxDecoration(color: Colors.grey),
          child: IconButton(
            onPressed: () {},
            icon: const Icon(Icons.account_circle),
          ),
        ),
        const ListTile(
          title: Text("Option 1"),
        ),
        const ListTile(
          title: Text("Option 2"),
        ),
      ],
    ),
  );
}
