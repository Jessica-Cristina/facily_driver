import 'package:flutter/material.dart';

class CustomPositioned extends StatelessWidget {
  final Color color;
  final List<Widget> children;
  const CustomPositioned(
      {Key? key, required this.color, required this.children})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      child: Container(
        padding: const EdgeInsets.all(10.0),
        decoration:
            BoxDecoration(borderRadius: BorderRadius.circular(5), color: color),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start, children: children),
      ),
    );
  }
}
