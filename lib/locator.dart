import 'package:friver_app/data/repositories/driver_repository.dart';
import 'package:friver_app/data/repositories/driver_repository_interface.dart';
import 'package:get_it/get_it.dart';

import 'data/services/http_service.dart';

var getIt = GetIt.instance;

void setupLocator() {
  getIt.registerLazySingleton(() => HttpService());

  getIt.registerLazySingleton<DriverRepositoryInterface>(
      () => DriverRepository(getIt.get<HttpService>()));
}
