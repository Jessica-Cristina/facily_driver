import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:friver_app/data/models/driver_load.dart';
import 'package:friver_app/data/models/driver_model.dart';
import 'package:friver_app/data/models/driver_pendency.dart';
import 'package:friver_app/data/models/driver_qrcode.dart';
import 'package:friver_app/data/repositories/driver_repository_interface.dart';

class DriverState {
  DriverModel? get props => null;
  DriverPendency? get pend => null;
  DriverLoad? get load => null;
  DriverQRCode? get qr => null;
}

class SuccessState extends DriverState {
  final DriverModel driver;

  SuccessState(this.driver);
  @override
  DriverModel? get props => driver;
}

class SuccessPendencyState extends DriverState {
  final DriverPendency pendency;

  SuccessPendencyState(this.pendency);

  @override
  DriverPendency? get pend => pendency;
}

class SuccesLoadState extends DriverState {
  final DriverLoad? loads;

  SuccesLoadState(this.loads);

  @override
  DriverLoad? get load => loads;
}

class SuccessQRCodeState extends DriverState {
  final DriverQRCode? qrc;

  SuccessQRCodeState(this.qrc);

  @override
  DriverQRCode? get qr => qrc;
}

class InicialState extends DriverState {
  @override
  DriverModel? get props => null;
}

class ErrorState extends DriverState {
  @override
  DriverModel? get props => null;
}

class LoadingState extends DriverState {
  @override
  DriverModel? get props => null;
}

class DriverModelCubit extends Cubit<DriverState> {
  final DriverRepositoryInterface _repository;
  DriverModelCubit(this._repository) : super(LoadingState()) {
    getPendency();
    getDriver();
    getDriverLoad();
  }

  Future<DriverModel?> getDriver() async {
    try {
      emit(LoadingState());
      final response = await _repository.getDriver();
      emit(SuccessState(response!));
    } on Exception {
      emit(ErrorState());
    }
  }

  Future<DriverPendency?> getPendency() async {
    try {
      emit(LoadingState());
      final pendency = await _repository.getDriverPendency();
      emit(SuccessPendencyState(pendency!));
    } on Exception {
      emit(ErrorState());
    }
  }

  Future<DriverLoad?> getDriverLoad() async {
    try {
      emit(LoadingState());
      final loads = await _repository.getDriverLoad();
      emit(SuccesLoadState(loads));
    } on Exception {
      emit(ErrorState());
    }
  }
}
