class DriverModel {
  String iD;
  String capKey;
  Caps caps;
  Data data;
  List<String> roles;

  DriverModel(
      {required this.iD,
      required this.capKey,
      required this.caps,
      required this.data,
      required this.roles});

  factory DriverModel.fromJson(Map<String, dynamic> json) {
    return DriverModel(
        iD: json['ID'],
        capKey: json['cap_key'],
        caps: Caps.fromJson(json['caps']),
        data: Data.fromJson(json['data']),
        roles: json['roles'].cast<String>());
  }
}

class Caps {
  bool administrator;
  bool expedition;
  bool expeditionAdmin;
  bool expeditionChecker;
  bool expeditionDefault;
  bool expeditionDispatcher;
  bool expeditionOrganizer;
  bool expeditionSeparator;
  bool facilyMerchants;
  bool logisticAdmin;
  bool logisticTransport;
  bool pickupLogistic;
  bool pickupLogisticDriver;
  bool production;
  bool supportAgent;

  Caps(
      {required this.administrator,
      required this.expedition,
      required this.expeditionAdmin,
      required this.expeditionChecker,
      required this.expeditionDefault,
      required this.expeditionDispatcher,
      required this.expeditionOrganizer,
      required this.expeditionSeparator,
      required this.facilyMerchants,
      required this.logisticAdmin,
      required this.logisticTransport,
      required this.pickupLogistic,
      required this.pickupLogisticDriver,
      required this.production,
      required this.supportAgent});

  factory Caps.fromJson(Map<String, dynamic> json) {
    return Caps(
      administrator: json['administrator'],
      expedition: json['expedition'],
      expeditionAdmin: json['expedition_admin'],
      expeditionChecker: json['expedition_checker'],
      expeditionDefault: json['expedition_default'],
      expeditionDispatcher: json['expedition_dispatcher'],
      expeditionOrganizer: json['expedition_organizer'],
      expeditionSeparator: json['expedition_separator'],
      facilyMerchants: json['facily_merchants'],
      logisticAdmin: json['logistic_admin'],
      logisticTransport: json['logistic_transport'],
      pickupLogistic: json['pickup_logistic'],
      pickupLogisticDriver: json['pickup_logistic_driver'],
      production: json['production'],
      supportAgent: json['support_agent'],
    );
  }
}

class Data {
  String iD;
  Address address;
  String avatar;
  String birthdate;
  String cpf;
  String displayName;
  String firstName;
  String lastName;
  Phone phone;
  PreferredAddress preferredAddress;
  String rateAsBeautyVendor;
  String rateAsUser;
  String refCode;
  String userActivationKey;
  String userEmail;
  String userLogin;
  String userNicename;
  String userRegistered;
  String userStatus;
  String userUrl;

  Data(
      {required this.iD,
      required this.address,
      required this.avatar,
      required this.birthdate,
      required this.cpf,
      required this.displayName,
      required this.firstName,
      required this.lastName,
      required this.phone,
      required this.preferredAddress,
      required this.rateAsBeautyVendor,
      required this.rateAsUser,
      required this.refCode,
      required this.userActivationKey,
      required this.userEmail,
      required this.userLogin,
      required this.userNicename,
      required this.userRegistered,
      required this.userStatus,
      required this.userUrl});

  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
      iD: json['ID'],
      address: Address.fromJson(json['address']),
      avatar: json['avatar'],
      birthdate: json['birthdate'],
      cpf: json['cpf'],
      displayName: json['display_name'],
      firstName: json['first_name'],
      lastName: json['last_name'],
      phone: Phone.fromJson(json['phone']),
      preferredAddress: PreferredAddress.fromJson(json['preferred_address']),
      rateAsBeautyVendor: json['rate_as_beauty_vendor'],
      rateAsUser: json['rate_as_user'],
      refCode: json['ref_code'],
      userActivationKey: json['user_activation_key'],
      userEmail: json['user_email'],
      userLogin: json['user_login'],
      userNicename: json['user_nicename'],
      userRegistered: json['user_registered'],
      userStatus: json['user_status'],
      userUrl: json['user_url'],
    );
  }
}

class Address {
  String address1;
  String address2;
  String city;
  String country;
  String neighborhood;
  String number;
  String postcode;
  String state;

  Address(
      {required this.address1,
      required this.address2,
      required this.city,
      required this.country,
      required this.neighborhood,
      required this.number,
      required this.postcode,
      required this.state});

  factory Address.fromJson(Map<String, dynamic> json) {
    return Address(
      address1: json['address_1'],
      address2: json['address_2'],
      city: json['city'],
      country: json['country'],
      neighborhood: json['neighborhood'],
      number: json['number'],
      postcode: json['postcode'],
      state: json['state'],
    );
  }
}

class Phone {
  String areaCode;
  String countryCode;
  String number;

  Phone(
      {required this.areaCode,
      required this.countryCode,
      required this.number});

  factory Phone.fromJson(Map<String, dynamic> json) {
    return Phone(
      areaCode: json['area_code'],
      countryCode: json['country_code'],
      number: json['number'],
    );
  }
}

class PreferredAddress {
  String address1;
  String address2;
  String city;
  String country;
  String neighborhood;
  String number;
  String state;

  PreferredAddress(
      {required this.address1,
      required this.address2,
      required this.city,
      required this.country,
      required this.neighborhood,
      required this.number,
      required this.state});

  factory PreferredAddress.fromJson(Map<String, dynamic> json) {
    return PreferredAddress(
      address1: json['address_1'],
      address2: json['address_2'],
      city: json['city'],
      country: json['country'],
      neighborhood: json['neighborhood'],
      number: json['number'],
      state: json['state'],
    );
  }
}
