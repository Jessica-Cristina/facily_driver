class DriverLoad {
  List<dynamic> items;
  int total;
  int page;
  int size;

  DriverLoad(
      {required this.items,
      required this.total,
      required this.page,
      required this.size});

  factory DriverLoad.fromJson(Map<String, dynamic> json) {
    return DriverLoad(
      items: json['items'],
      total: json['total'],
      page: json['page'],
      size: json['size'],
    );
  }
}

class Items {
  int id;
  int driverId;
  String loadedAt;
  bool accepted;
  String acceptedAt;
  int dispatchedBoxes;
  int userId;

  Items(
      {required this.id,
      required this.driverId,
      required this.loadedAt,
      required this.accepted,
      required this.acceptedAt,
      required this.dispatchedBoxes,
      required this.userId});

  factory Items.fromJson(Map<String, dynamic> json) {
    return Items(
      id: json['id'],
      driverId: json['driver_id'],
      loadedAt: json['loaded_at'],
      accepted: json['accepted'],
      acceptedAt: json['accepted_at'],
      dispatchedBoxes: json['dispatched_boxes'],
      userId: json['user_id'],
    );
  }
}
