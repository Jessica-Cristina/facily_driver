class DriverPendency {
  bool blocklisted;
  int boxes;
  List<int> orders;

  DriverPendency(
      {required this.blocklisted, required this.boxes, required this.orders});

  factory DriverPendency.fromJson(Map<String, dynamic> json) {
    return DriverPendency(
      blocklisted: json['blocklisted'],
      boxes: json['boxes'],
      orders: json['orders'].cast<int>(),
    );
  }
}
