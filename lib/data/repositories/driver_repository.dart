import 'package:friver_app/data/models/driver_pendency.dart';
import 'package:friver_app/data/models/driver_model.dart';
import 'package:friver_app/data/models/driver_load.dart';
import 'package:friver_app/data/repositories/driver_repository_interface.dart';
import 'package:friver_app/data/services/http_service.dart';
import 'package:friver_app/shared/strings.dart';

class DriverRepository extends DriverRepositoryInterface {
  final HttpService _http;
  final _baseUrl = Strings.driverApiUrl;
  final _id = Strings.driverId;
  final _headers = {'Authorization': Strings.token};

  DriverRepository(this._http);

  @override
  Future<DriverModel?> getDriver() async {
    String uri = '$_baseUrl/users-me';
    var response = await _http.getRequest(uri, headers: _headers);
    if (response.success) {
      DriverModel data = DriverModel.fromJson(response.content!);
      return data;
    }
    return null;
  }

  @override
  Future<DriverLoad?> getDriverLoad() async {
    String uri = '$_baseUrl/load-truck';
    var response = await _http.getRequest(uri,
        params: {'driver_id': _id}, headers: _headers);
    if (response.success) {
      DriverLoad data = DriverLoad.fromJson(response.content!);
      return data;
    }
    return null;
  }

  @override
  Future<DriverPendency?> getDriverPendency() async {
    String uri = '$_baseUrl/driver-pendency';
    var response = await _http.getRequest(uri,
        params: {'driver_id': _id}, headers: _headers);
    if (response.success) {
      DriverPendency data = DriverPendency.fromJson(response.content!);
      return data;
    }
    return null;
  }
}
