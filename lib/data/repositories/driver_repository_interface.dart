import 'package:friver_app/data/models/driver_load.dart';
import 'package:friver_app/data/models/driver_model.dart';
import 'package:friver_app/data/models/driver_pendency.dart';

abstract class DriverRepositoryInterface {
  Future<DriverModel?> getDriver();
  Future<DriverPendency?> getDriverPendency();
  Future<DriverLoad?> getDriverLoad();
}
