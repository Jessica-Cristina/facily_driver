import 'package:flutter/material.dart';
import 'package:friver_app/locator.dart';
import 'package:friver_app/ui/pages/home_page.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  setupLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        textTheme: GoogleFonts.poppinsTextTheme(
          Theme.of(context).textTheme,
        ),
      ),
      title: 'Facily Driver',
      debugShowCheckedModeBanner: false,
      home: const HomePage(),
      initialRoute: HomePage.name,
      routes: {
        HomePage.name: (_) => const HomePage(),
      },
    );
  }
}
